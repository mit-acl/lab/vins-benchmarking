Snapdragon Setup
=================
This section contains a detailed procedure for setting up both VINS-Fusion and OpenVINS on the Snapdragon (refer to VINSFusion_Setup and OpenVINS_Setup). I was not able to get realtime performance from VINS-Fusion on the Snapdragon, but did get realtime performance with OpenVINS. Running OpenVINS on the Snapdragon used about 150% CPU, and running the ROS wrapper for the IMU and camera used another 150% CPU. Note that the Snapdragon has 4 cores (i.e. max CPU is 400%). 

There are three modified VINS-Fusion repos included here. VINS-Fusion had to be modified in three main ways. The first was to change the IMU callback to use the Snapdragon's IMU, the second was to use OpenCV fisheye undistortion functions to add rectification support for the Snapdragon's fisheye camera, and the third was adding fixes to prevent compile errors special to the Snapdragon. 

VINSFusion_for_snapdragon is meant to be run on a laptop and contains a rectification node which will rectify the fisheye images and package the IMU into messages normally processed by VINS-Fusion. This serves as a test that images are being properly rectified. Note that when we put VINS-Fusion on the Snapdragon, we only want to recitfy feature points and not the whole image in order to efficiently compute pose. This package is merly a way to test performance on Snapdragon data.

VINSFusion_compiles_on_snapdragon contains code fixes to allow VINSFusion to compile on the Snapdragon. This repo is not setup to handle data from the Snapdragon's sensors. Instead it is intended to test datasets used as EUROC and d435i.

Finally, the folder "VINSFusion" contains VINS-Fusion modified to compile on the Snapdragon and run Snapdragon data. Note that there is no recification node. The camera models in VINS-Fusion have been modified to directly rectify the feature points. Additionally, the Snapdragon's IMU data is directly handled by the modified version of VINS-Fusion.
