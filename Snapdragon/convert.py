from evo.tools import file_interface
traj = file_interface.read_tum_trajectory_file("odometry.tum")
traj.timestamps = traj.timestamps / 1e9
file_interface.write_tum_trajectory_file("odometry.tum", traj)
