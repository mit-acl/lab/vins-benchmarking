First step to set up the realsense software is to install SDK. This project uses SDK 2.0 (build 2.29.0) which can be found in the ROS-wrapper Repo in the following section. The driver installation can be checked by running 'realsense-viewer'

Install the ROS-wrapper supplied by Intel "https://github.com/IntelRealSense/realsense-ro". This project is currently using the 2.2.9 develoment branch. 

The IR node must be disabled. In order to do such, simply run the following command rosrun "rqt_reconfigure rqt_reconfigure" and deselect "emitter_enabled"
