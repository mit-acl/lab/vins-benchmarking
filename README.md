VINS Benchmarking
=================

This repo serves two main purposes. The first is to present a background of hardware and methods used for benchmarking open source visual-inertial navigation systems, and the second is to describe in detail how to setup a Snapdragon Flight board to run an open source VIO in realtime.

The procedure for using the Snapdragon can be found in the Snapdragon section of this repo.

The benchmarking part of this project is focused on evaluating the performance of various VIO algoritms on multiple processors. Some tests will use a realsense d435i camera which features a stereo pair, rgb camera, and IMU. Since our comparision is focused on fusion of a mono camera with IMU, only the left stereo lens and IMU of the camera were used. This project is based on A Benchmark Comparison of Monocular Visual-Inertial OdometryAlgorithms for Flying Robots, but our intent is to apply the comparision to additional VIOs, processors, and datasets that are directly relevant for the ACL. Current processors are: NUC, TX2, and Snapdragon. Current datasets are: EUROC and d435i data collected in the building 31 VICON room. Current VIOs are VINS-Fusion and OpenVINS.

Comparisions are being based upon accuracy, CPU usage, and ease of use. For accuracy comparison VICON is used as ground truth.

Accuracy comparision is done using EVO "https://github.com/MichaelGrupp/evo" and we are currently using Absolute Pose Error (APE)  and Relative Pose Error (RPE). APE measures error in meters wheras RPE is a nondimension measure of error per distance traveled. In order to generate a comparision between ground truth and VIO estimate, EVO must allign the two trajectories into the same coordinate frame by finding the transformation and translation between the each trajectory's respective coordinate system. Details of this can be found in the EVO wiki, but from a high level, the transformation and translation can be done in one of three way: 
1. Optimizing over all timesteps
2. Optimizing over a set n timesteps
3. Alligning the initial pose of VIO estimate and ground truth

Although 1 is currently being used, for this project 1 may overfit the pose correction. 3 on the otherhand may not suffice to determine a precise pose correction. Thus, a future step for this project is to use EVO with the second method.

A simple mount for the camera and NUC was made from a piece of acrylic. <img src="vins_setup.jpg" width="200" height="150" />

__General notes and common concepts__  
Currently for the NUC, Ubuntu 16.04 is being used along with Kernal 4.15 and ROS Kinetic. The ROS package can be checked by running 'which roslaunch'. Note that for most ROS package add-ons that are relevant for this project and include "Melodic" or some version of ROS other than "Kinetic", you can simply replace the version with "Kinetic" in the command line and proceed as normal.

__Using Kalibr__

https://www.youtube.com/watch?v=_2S7gAnsP6A&feature=youtu.be

__Creating a workspace (reccomend using catkin build)__   
https://catkin-tools.readthedocs.io/en/latest/cheat_sheet.html Note: do NOT try and run catkin build and catkin_make in the same workspace

__Running a Jupyter Notebook from the command line__  
To clone from git - click on "RAW", save using Ctrl+S, remove .txt from file if it appears, and confirm save.

__Install jupyter using "pip install jupyterlab"__  
To open a notebook from the terminal, run 'jupyter notebook <your notebook>'

__Usefull ROS commands (beyond the most basic)__  
rqt_image_view - displays image from ROS topic  
rgt_bag - convenient way to check contents of a ROS bag  
rqt_graph - visual display of nodes and topics
